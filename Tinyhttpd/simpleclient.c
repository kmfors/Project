#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>

int main(int argc, char *argv[])
{
    int sockfd;                     // 套接字
    int len;                        // 长度
    struct sockaddr_in address;     // 套接地址
    int result;                     // 结果
    char ch = 'A';                  // 字符

    //ipv4方式、tcp连接方式生成一个套接字
    sockfd = socket(AF_INET, SOCK_STREAM, 0);           

    // 初始化套接地址
    address.sin_family = AF_INET;   // 协议族
    address.sin_addr.s_addr = inet_addr("127.0.0.1");   // ip 地址
    address.sin_port = htons(9734); // 端口号：主机号变为网络号
    len = sizeof(address);          // 套接地址变量的字节大小
    result = connect(sockfd, (struct sockaddr *)&address, len); 

    if (result == -1)
    {
        perror("oops: client1");
        exit(1);
    }
    write(sockfd, &ch, 1);  // 发送一个字符
    read(sockfd, &ch, 1);   // 读取一个字符
    printf("char from server = %c\n", ch);  // 打印该字符
    close(sockfd);          // 关闭套接字
    exit(0);
}
